package com.gegidze.vending_machine.integrationTest.user;

import com.gegidze.vending_machine.Dto.request.RegisterUserDTO;
import com.gegidze.vending_machine.Dto.request.UpdateUserDTO;
import com.gegidze.vending_machine.Dto.response.UserDTO;
import com.gegidze.vending_machine.VendingMachineApplication;
import com.gegidze.vending_machine.enums.UserRole;
import com.gegidze.vending_machine.enums.UserStatus;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Objects;

@SpringBootTest(classes = VendingMachineApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserControllerIT {
    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testRegisterBuyerUser() throws URISyntaxException {
        String baseUrl = "http://localhost:" + port + "/user";
        URI uri = new URI(baseUrl);
        HttpHeaders headers = new HttpHeaders();
        RegisterUserDTO registerUserDTO = new RegisterUserDTO()
                .setUsername("sobhan")
                .setPassword("123456")
                .setRole(UserRole.BUYER);
        HttpEntity request = new HttpEntity(registerUserDTO, headers);
        ResponseEntity<UserDTO> response = restTemplate.exchange(
                uri,
                HttpMethod.POST,
                request,
                new ParameterizedTypeReference<UserDTO>() {
                }
        );

        UserDTO responseBody = Objects.requireNonNull(response.getBody());
        Assertions.assertThat(responseBody.getUsername()).isEqualTo(registerUserDTO.getUsername());
        Assertions.assertThat(responseBody.getRole()).isEqualTo(registerUserDTO.getRole());
        Assertions.assertThat(responseBody.getStatus()).isEqualTo(UserStatus.ACTIVE);
    }

    @Test
    public void testGetUser() throws URISyntaxException {
        String baseUrl = "http://localhost:" + port + "/user/sobhan";
        URI uri = new URI(baseUrl);
        HttpHeaders headers = new HttpHeaders();
        HttpEntity request = new HttpEntity(headers);
        ResponseEntity<UserDTO> response = restTemplate.exchange(
                uri,
                HttpMethod.GET,
                request,
                new ParameterizedTypeReference<UserDTO>() {
                }
        );

        UserDTO responseBody = Objects.requireNonNull(response.getBody());
        Assertions.assertThat(responseBody.getUsername()).isEqualTo("sobhan");
        Assertions.assertThat(responseBody.getRole()).isEqualTo(UserRole.BUYER);
        Assertions.assertThat(responseBody.getStatus()).isEqualTo(UserStatus.ACTIVE);
    }

    @Test
    public void testRegisterSellerUser() throws URISyntaxException {
        String baseUrl = "http://localhost:" + port + "/user";
        URI uri = new URI(baseUrl);
        HttpHeaders headers = new HttpHeaders();
        RegisterUserDTO registerUserDTO = new RegisterUserDTO()
                .setUsername("mohsen")
                .setPassword("123456")
                .setRole(UserRole.SELLER);
        HttpEntity request = new HttpEntity(registerUserDTO, headers);
        ResponseEntity<UserDTO> response = restTemplate.exchange(
                uri,
                HttpMethod.POST,
                request,
                new ParameterizedTypeReference<UserDTO>() {
                }
        );

        UserDTO responseBody = Objects.requireNonNull(response.getBody());
        Assertions.assertThat(responseBody.getUsername()).isEqualTo(registerUserDTO.getUsername());
        Assertions.assertThat(responseBody.getRole()).isEqualTo(registerUserDTO.getRole());
        Assertions.assertThat(responseBody.getStatus()).isEqualTo(UserStatus.ACTIVE);
    }

//    @Test  //TODO: calling with postman has no error but it returns null!!
    public void testUpdateUserRoleToBuyer() throws URISyntaxException {
        String baseUrl = "http://localhost:" + port + "/user";
        URI uri = new URI(baseUrl);
        HttpHeaders headers = new HttpHeaders();
        UpdateUserDTO updateUserDTO = new UpdateUserDTO()
                .setUsername("mohsen")
                .setPassword("123456")
                .setRole(UserRole.BUYER);
        HttpEntity request = new HttpEntity(updateUserDTO, headers);
        ResponseEntity<UserDTO> response = restTemplate.exchange(
                uri,
                HttpMethod.PUT,
                request,
                new ParameterizedTypeReference<UserDTO>() {
                }
        );

        UserDTO responseBody = Objects.requireNonNull(response.getBody());
        Assertions.assertThat(responseBody.getUsername()).isEqualTo(updateUserDTO.getUsername());
        Assertions.assertThat(responseBody.getRole()).isEqualTo(updateUserDTO.getRole());
        Assertions.assertThat(responseBody.getStatus()).isEqualTo(UserStatus.ACTIVE);
    }

    @Test
    public void testRegisterUserForDelete() throws URISyntaxException {
        String baseUrl = "http://localhost:" + port + "/user";
        URI uri = new URI(baseUrl);
        HttpHeaders headers = new HttpHeaders();
        RegisterUserDTO registerUserDTO = new RegisterUserDTO()
                .setUsername("delete")
                .setPassword("123456")
                .setRole(UserRole.BUYER);
        HttpEntity request = new HttpEntity(registerUserDTO, headers);
        ResponseEntity<UserDTO> response = restTemplate.exchange(
                uri,
                HttpMethod.POST,
                request,
                new ParameterizedTypeReference<UserDTO>() {
                }
        );

        UserDTO responseBody = Objects.requireNonNull(response.getBody());
        Assertions.assertThat(responseBody.getUsername()).isEqualTo(registerUserDTO.getUsername());
        Assertions.assertThat(responseBody.getRole()).isEqualTo(registerUserDTO.getRole());
        Assertions.assertThat(responseBody.getStatus()).isEqualTo(UserStatus.ACTIVE);
    }

    @Test
    public void testDeleteUser() throws URISyntaxException {
        String baseUrl = "http://localhost:" + port + "/user/delete";
        URI uri = new URI(baseUrl);
        HttpHeaders headers = new HttpHeaders();
        HttpEntity request = new HttpEntity(headers);
        ResponseEntity<UserDTO> response = restTemplate.exchange(
                uri,
                HttpMethod.DELETE,
                request,
                new ParameterizedTypeReference<UserDTO>() {
                }
        );

        UserDTO responseBody = Objects.requireNonNull(response.getBody());
        Assertions.assertThat(responseBody.getUsername()).isEqualTo("delete");
        Assertions.assertThat(responseBody.getRole()).isEqualTo(UserRole.BUYER);
        Assertions.assertThat(responseBody.getStatus()).isEqualTo(UserStatus.INACTIVE);
    }
}
