package com.gegidze.vending_machine.util;

/**
 * @author S.shakery
 */

public class Constant {

    public static String STATUS_CODE(int code) {
        return String.format("statusCodes.%d", code);
    }
    public static final String REGISTER_USER_REQUEST_INVALID = "invalid.request.user.register";
    public static final String REGISTER_USER_USERNAME_EMPTY = "empty.username.user.register";
    public static final String USER_USERNAME_DUPLICATE = "duplicate.username.user";
    public static final String USER_USERNAME_NOT_FOUND = "notFound.username.user";
    public static final String PRODUCT_NOT_FOUND = "notFound.product";
    public static final String INVALID_PRODUCT_OWNER = "invalid.product.owner";
    public static final String REGISTER_USER_PASSWORD_EMPTY = "empty.password.user.register";
    public static final String USER_ROLE_INVALID = "invalid.role.user";
    public static final String SELLER_USER_ROLE_REQUIRED = "required.user.role.seller";
    public static final String BUYER_USER_ROLE_REQUIRED = "required.user.role.buyer";
    public static final String DEPOSIT_AMOUNT_MULTIPLE_OF_FIVE_REQUIRED = "required.multipleOfFive.depositAmount";
    public static final String DEPOSIT_REQUIRED = "required.deposit";
    public static final String USER_ROLE_NULL = "null.role.user";
    public static final String UPDATE_USER_REQUEST_INVALID = "invalid.request.user.update";
    public static final String UPDATE_USER_USERNAME_EMPTY = "empty.username.user.update";
    public static final String UPDATE_USER_PASSWORD_EMPTY = "empty.password.user.update";






}
