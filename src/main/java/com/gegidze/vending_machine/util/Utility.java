package com.gegidze.vending_machine.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.sql.Timestamp;
import java.util.Map;

public class Utility {

    private static final ObjectMapper _ObjectMapper = new ObjectMapper();

    public static boolean isNullOrEmpty(String data) {
        return data == null || data.trim().equals("");
    }

    public static String toJson(Object object) throws JsonProcessingException {
        return _ObjectMapper.writer().withDefaultPrettyPrinter().writeValueAsString(object);
    }

    public static Timestamp getCurrentDate() {
        return new Timestamp(System.currentTimeMillis());
    }

    public static boolean isValidJSON(final String json) {
        boolean valid = true;
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.readTree(json);
        } catch (JsonProcessingException e) {
            valid = false;
        }
        return valid;
    }

    public static String convertToJson(Object json) {
        if (json == null) return "{}";
        try {
            String pretty = _ObjectMapper.writer().withDefaultPrettyPrinter().writeValueAsString(json);
            return addIndent(pretty);
        } catch (JsonProcessingException e) {
            return "{}";
        }
    }

    public static String prettyJson(String uglyJson) {
        try {
            Map json = _ObjectMapper.readValue(uglyJson, Map.class);
            json.remove("eauth");
            String prettyJson = _ObjectMapper.writer().withDefaultPrettyPrinter().writeValueAsString(json);
            return addIndent(prettyJson);
        } catch (Exception e) {
            return uglyJson;
        }

    }

    private static String addIndent(String string) {
        return string.replaceAll("\n", "\n\t");
    }

}
