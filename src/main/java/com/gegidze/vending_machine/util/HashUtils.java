package com.gegidze.vending_machine.util;


import org.bouncycastle.crypto.generators.Argon2BytesGenerator;
import org.bouncycastle.crypto.params.Argon2Parameters;

/**
 * @author S.shakery
 */

public class HashUtils {

    public static byte[] generatePasswordHash(byte[] salt, String password) {
        //Hash password
        Argon2Parameters.Builder builder = new Argon2Parameters.Builder(Argon2Parameters.ARGON2_id)
                .withVersion(Argon2Parameters.ARGON2_VERSION_13)
                .withIterations(96)
                .withMemoryPowOfTwo(8)
                .withParallelism(8)
                .withSalt(salt);
        Argon2BytesGenerator gen = new Argon2BytesGenerator();
        gen.init(builder.build());
        byte[] result = new byte[32];
        gen.generateBytes(
                password.toCharArray(),
                result,
                0, result.length);
        return result;
    }
}
