package com.gegidze.vending_machine.enums;

/**
 * @Author: s.shakeri
 * at 6/5/2022
 **/

public enum DepositStatus {
    CURRENT(0),
    OLD(1);

    private final int val;

    DepositStatus(final int val) {
        this.val = val;
    }

    public int getVal() {
        return val;
    }
}
