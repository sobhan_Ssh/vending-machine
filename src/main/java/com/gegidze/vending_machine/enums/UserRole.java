package com.gegidze.vending_machine.enums;

import com.gegidze.vending_machine.exception.BadRequestException;
import com.gegidze.vending_machine.exception.BaseException;
import com.gegidze.vending_machine.util.Constant;

/**
 * @Author: s.shakeri
 * at 6/4/2022
 **/

public enum UserRole {

    BUYER(0),
    SELLER(1);
    private final int val;

    UserRole(final int val) {
        this.val = val;
    }

    public int getVal() {
        return val;
    }

    public static boolean contains(int value) {
        // Check if enum contain text then return true else return false
        for (UserRole role : UserRole.values()) {
            if (role.getVal() == value) {
                return true;
            }
        }
        return false;
    }

    public static UserRole parseUserRole(byte status) {
        if (status == UserRole.BUYER.getVal())
            return UserRole.BUYER;
        else if (status == UserRole.SELLER.getVal())
            return UserRole.SELLER;
        else
            throw new BadRequestException(new BaseException.ErrorDetail()
                    .setLoc("userRole")
                    .setType(Constant.USER_ROLE_INVALID));
    }

}
