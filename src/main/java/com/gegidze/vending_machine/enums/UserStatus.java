package com.gegidze.vending_machine.enums;

/**
 * @Author: s.shakeri
 * at 6/4/2022
 **/

public enum UserStatus {

    ACTIVE(0),
    INACTIVE(1);

    private final int val;

    UserStatus(final int val) {
        this.val = val;
    }

    public int getVal() {
        return val;
    }
}
