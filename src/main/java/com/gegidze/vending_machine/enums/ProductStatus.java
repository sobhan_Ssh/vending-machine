package com.gegidze.vending_machine.enums;

/**
 * @Author: s.shakeri
 * at 6/5/2022
 **/

public enum ProductStatus {
    ACTIVE(0),
    INACTIVE(1);

    private final int val;

    ProductStatus(final int val) {
        this.val = val;
    }

    public int getVal() {
        return val;
    }
}
