package com.gegidze.vending_machine.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class SsoConfig {

    @Value("${sso.config.key}")
    private String privateKey;

    @Value("${sso.config.expiration}")
    private Long expiration;

    @Value("${sso.config.refreshExpire}")
    private Long refreshExpire;

    @Value("${sso.config.audience}")
    private String audiences;

    @Value("${sso.config.issuer}")
    private String issuer;

    @Value("${sso.config.credential}")
    private String credentials;

    public String getPrivateKey() {
        return privateKey;
    }

    public String getAudiences() {
        return audiences;
    }

    public String getIssuer() {
        return issuer;
    }

    public String getCredentials() {
        return credentials;
    }

    public Long getExpiration() {
        return expiration;
    }

    public Long getRefreshExpire() {
        return refreshExpire;
    }
}
