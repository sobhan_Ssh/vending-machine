package com.gegidze.vending_machine.config;

import org.jose4j.jwa.AlgorithmConstraints;
import org.jose4j.jwk.JsonWebKeySet;
import org.jose4j.jwk.PublicJsonWebKey;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.keys.resolvers.JwksVerificationKeyResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

@Component
public class BeanConfig {

    @Bean
    public JwtConsumer jwtConsumer(JwtConfig jwtConfig) throws Exception {
        KeyFactory kf = KeyFactory.getInstance("RSA");
        JsonWebKeySet jsonWebKeySet = new JsonWebKeySet();
        String key = jwtConfig.getPublicKey();

        PublicKey publicKey = kf.generatePublic(new X509EncodedKeySpec(Base64.getDecoder().decode(key)));
        PublicJsonWebKey publicJsonWebKey = PublicJsonWebKey.Factory.newPublicJwk(publicKey);
        publicJsonWebKey.setKeyId(key);
        jsonWebKeySet.addJsonWebKey(publicJsonWebKey);
        JwksVerificationKeyResolver jwkResolver = new JwksVerificationKeyResolver(jsonWebKeySet.getJsonWebKeys());
        return new JwtConsumerBuilder()
                .setRequireExpirationTime() // the JWT must have an expiration time
                .setAllowedClockSkewInSeconds(30) // allow some leeway in validating time based claims to account for clock skew
                .setRequireSubject()
                .setExpectedIssuers(true, jwtConfig.getIssuer()) // whom the JWT needs to have been issued by
                .setExpectedAudience(jwtConfig.getAudience()) // to whom the JWT is intended for
                .setVerificationKeyResolver(jwkResolver) // verify the signature with the public key
                .setJwsAlgorithmConstraints( // only allow the expected signature algorithm(s) in the given context
                        AlgorithmConstraints.ConstraintType.PERMIT, AlgorithmIdentifiers.RSA_USING_SHA256) // which is only RS256 here
                .build();
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry
                        .addMapping("/**")
                        .allowedHeaders("*")
                        .allowedMethods("*");
            }
        };
    }
}
