/*
package com.gegidze.vending_machine.config;


import com.gegidze.vending_machine.Dto.request.BaseRequest;
import com.gegidze.vending_machine.exception.BaseException;
import com.gegidze.vending_machine.exception.ForbiddenException;
import com.gegidze.vending_machine.exception.InternalServerException;
import com.gegidze.vending_machine.exception.UnauthorizedException;
import com.gegidze.vending_machine.util.Utility;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

import java.util.Map;

@Order(Ordered.HIGHEST_PRECEDENCE)
@Aspect
@Configuration
public class processJWTAspect {

    @Autowired
    JwtConsumer jwtConsumer;

    @Pointcut("execution(public * *(..))")
    public void publicMethod() {
    }

    @Pointcut("within(@org.springframework.web.bind.annotation.RestController *)")
    public void beanAnnotatedWithRestController() {
    }

    @Around("publicMethod() " +
            "&& beanAnnotatedWithRestController() " +
            "&& !@annotation(com.gegidze.vending_machine.annotation.AllowAnonymous)")
    public Object authorization(ProceedingJoinPoint pjp) throws Throwable {
        Object[] exArgs = pjp.getArgs();
        Map<String, String> headers;
        String jwt = "";
        JwtClaims jwtClaims = null;

        if (exArgs[0] instanceof Map) {
            try {
                headers = (Map<String, String>) exArgs[0];
                jwt = headers.get("Authorization");
                if (Utility.isNullOrEmpty(jwt)) jwt = headers.get("authorization");
                jwt = jwt.substring(7);
                jwtClaims = jwtConsumer.processToClaims(jwt);
            } catch (InvalidJwtException e) {
                jwtClaims = e.getJwtContext().getJwtClaims();
                if (e.hasExpired()) {
                    throw new UnauthorizedException().addDetail(new BaseException.ErrorDetail()
                            .setLoc("token")
                            .setType("expired"));
                } else {
                    throw new ForbiddenException();
                }
            } catch (Exception ex) {
                throw new UnauthorizedException().addDetail(new BaseException.ErrorDetail()
                        .setLoc("token")
                        .setType("invalid"));
            }
        } else {
            throw new InternalServerException();
        }

        if (jwtClaims != null) {
            int i = 0;
            for (Object arg : pjp.getArgs()) {
                i++;
                // Fill base request
                if (arg instanceof BaseRequest) {
                    BaseRequest req = arg == null ? new BaseRequest() : (BaseRequest) arg;
                    req.getUser().setUsername(jwtClaims.getSubject());
                }
            }
        }
        return pjp.proceed(exArgs);
    }

}
*/
