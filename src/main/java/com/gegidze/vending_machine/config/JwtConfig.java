package com.gegidze.vending_machine.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public final class JwtConfig {

    @Value("${jwt.config.key}")
    private String publicKey;

    @Value("${jwt.config.expiration}")
    private Long expiration;

    @Value("${jwt.config.audience}")
    private String audience;

    @Value("${jwt.config.issuer}")
    private String issuer;

    @Value("${jwt.config.credentials}")
    private String credentials;

    public String getPublicKey() {
        return publicKey;
    }

    public Long getExpiration() {
        return expiration;
    }

    public String getAudience() {
        return audience;
    }

    public String getIssuer() {
        return issuer;
    }

    public String getCredentials() {
        return credentials;
    }
}
