package com.gegidze.vending_machine.repository;

import com.gegidze.vending_machine.repository.entity.Product;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @Author: s.shakeri
 * at 6/4/2022
 **/

@Repository
public interface ProductRepository extends PagingAndSortingRepository<Product, Long> {

    Optional<Product> findByProductName(String productName);

    Optional<Product> findById(Long id);

}
