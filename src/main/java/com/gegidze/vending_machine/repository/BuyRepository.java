package com.gegidze.vending_machine.repository;

import com.gegidze.vending_machine.repository.entity.Buy;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @Author: s.shakeri
 * at 6/4/2022
 **/

@Repository
public interface BuyRepository extends CrudRepository<Buy, Long> {
}
