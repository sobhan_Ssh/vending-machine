package com.gegidze.vending_machine.repository.entity;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * @Author: s.shakeri
 * at 6/4/2022
 **/

@Entity
@Table(name = "BUY")
public class Buy {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private Long id;

    @Column(name = "DATE_TIME")
    private Timestamp dateTime;

    @ManyToOne(optional = false)
    @JoinColumn(name = "user_id", nullable = false,
            referencedColumnName = "id")
    private User user;

    @Column(name = "AMOUNT")
    private Long amount;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "product_id", referencedColumnName = "id", nullable = false)
    private Product product;

    public Long getId() {
        return id;
    }

    public Buy setId(Long id) {
        this.id = id;
        return this;
    }

    public Timestamp getDateTime() {
        return dateTime;
    }

    public Buy setDateTime(Timestamp dateTime) {
        this.dateTime = dateTime;
        return this;
    }

    public User getUser() {
        return user;
    }

    public Buy setUser(User user) {
        this.user = user;
        return this;
    }

    public Long getAmount() {
        return amount;
    }

    public Buy setAmount(Long amount) {
        this.amount = amount;
        return this;
    }

    public Product getProduct() {
        return product;
    }

    public Buy setProduct(Product product) {
        this.product = product;
        return this;
    }
}
