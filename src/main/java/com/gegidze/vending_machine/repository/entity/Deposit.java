package com.gegidze.vending_machine.repository.entity;

import com.gegidze.vending_machine.enums.DepositStatus;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * @Author: s.shakeri
 * at 6/4/2022
 **/

@Entity
@Table(name = "DEPOSIT")
public class Deposit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private Long id;

    @Column(name = "DATE_TIME")
    private Timestamp dateTime;

    @Column(name = "DEPOSIT_AMOUNT", nullable = false)
    private Long depositAmount;

    @Column(name = "PREVIOUS_AMOUNT")
    private Long previousAmount;

    @Column(name = "TOTAL_AMOUNT", nullable = false)
    private Long totalAmount;

    @ManyToOne(optional = false)
    @JoinColumn(name = "user_id", nullable = false,
            referencedColumnName = "id")
    private User user;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "STATUS", nullable = false)
    private DepositStatus status;

    public Long getId() {
        return id;
    }

    public Deposit setId(Long id) {
        this.id = id;
        return this;
    }

    public Timestamp getDateTime() {
        return dateTime;
    }

    public Deposit setDateTime(Timestamp dateTime) {
        this.dateTime = dateTime;
        return this;
    }

    public User getUser() {
        return user;
    }

    public Deposit setUser(User user) {
        this.user = user;
        return this;
    }

    public Long getDepositAmount() {
        return depositAmount;
    }

    public Deposit setDepositAmount(Long depositAmount) {
        this.depositAmount = depositAmount;
        return this;
    }

    public Long getPreviousAmount() {
        return previousAmount;
    }

    public Deposit setPreviousAmount(Long previousAmount) {
        this.previousAmount = previousAmount;
        return this;
    }

    public Long getTotalAmount() {
        return totalAmount;
    }

    public Deposit setTotalAmount(Long totalAmount) {
        this.totalAmount = totalAmount;
        return this;
    }

    public DepositStatus getStatus() {
        return status;
    }

    public Deposit setStatus(DepositStatus status) {
        this.status = status;
        return this;
    }
}
