package com.gegidze.vending_machine.repository.entity;

import com.gegidze.vending_machine.enums.ProductStatus;

import javax.persistence.*;

/**
 * @Author: s.shakeri
 * at 6/4/2022
 **/

@Entity
@Table(name = "PRODUCT")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private Long id;

    @Column(name = "COUNT", nullable = false)
    private Long availableAmount;

    @Column(name = "COST", nullable = false)
    private Long cost;

    @Column(name = "NAME", nullable = false)
    private String productName;

    @ManyToOne(optional = false)
    @JoinColumn(name = "user_id", nullable = false,
            referencedColumnName = "id")
    private User user;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "STATUS", nullable = false)
    private ProductStatus status;

    public Long getId() {
        return id;
    }

    public Product setId(Long id) {
        this.id = id;
        return this;
    }

    public Long getAvailableAmount() {
        return availableAmount;
    }

    public Product setAvailableAmount(Long availableAmount) {
        this.availableAmount = availableAmount;
        return this;
    }

    public Long getCost() {
        return cost;
    }

    public Product setCost(Long cost) {
        this.cost = cost;
        return this;
    }

    public String getProductName() {
        return productName;
    }

    public Product setProductName(String productName) {
        this.productName = productName;
        return this;
    }

    public User getUser() {
        return user;
    }

    public Product setUser(User user) {
        this.user = user;
        return this;
    }

    public ProductStatus getStatus() {
        return status;
    }

    public Product setStatus(ProductStatus status) {
        this.status = status;
        return this;
    }
}
