package com.gegidze.vending_machine.repository.entity;

import com.gegidze.vending_machine.enums.UserRole;
import com.gegidze.vending_machine.enums.UserStatus;

import javax.persistence.*;
import java.util.List;

/**
 * @Author: s.shakeri
 * at 6/4/2022
 **/

@Entity
@Table(name = "USERS")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, nullable = false)
    private Long id;

    @Column(name = "USERNAME", unique = true, nullable = false)
    private String username;

    @Column(name = "PASSWORD", nullable = false)
    private String password;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "ROLE", nullable = false)
    private UserRole role;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "STATUS", nullable = false)
    private UserStatus status;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Product> products;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Deposit> deposits;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Buy> buys;

    @Column(name = "SALT")
    private String salt;

    public Long getId() {
        return id;
    }

    public User setId(Long id) {
        this.id = id;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public User setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public User setPassword(String password) {
        this.password = password;
        return this;
    }

    public UserRole getRole() {
        return role;
    }

    public User setRole(UserRole role) {
        this.role = role;
        return this;
    }

    public UserStatus getStatus() {
        return status;
    }

    public User setStatus(UserStatus status) {
        this.status = status;
        return this;
    }

    public List<Product> getProducts() {
        return products;
    }

    public User setProducts(List<Product> products) {
        this.products = products;
        return this;
    }

    public List<Deposit> getDeposits() {
        return deposits;
    }

    public User setDeposits(List<Deposit> deposits) {
        this.deposits = deposits;
        return this;
    }

    public List<Buy> getBuys() {
        return buys;
    }

    public User setBuys(List<Buy> buys) {
        this.buys = buys;
        return this;
    }

    public String getSalt() {
        return salt;
    }

    public User setSalt(String salt) {
        this.salt = salt;
        return this;
    }
}
