package com.gegidze.vending_machine.repository;

import com.gegidze.vending_machine.repository.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @Author: s.shakeri
 * at 6/4/2022
 **/

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    Optional<User> findByUsername(String username);

}
