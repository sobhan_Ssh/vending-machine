package com.gegidze.vending_machine.repository;

import com.gegidze.vending_machine.enums.DepositStatus;
import com.gegidze.vending_machine.repository.entity.Deposit;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @Author: s.shakeri
 * at 6/4/2022
 **/

@Repository
public interface DepositRepository extends CrudRepository<Deposit, Long> {

    Optional<Deposit> findByUser_IdAndStatus(Long userId, DepositStatus status);
}
