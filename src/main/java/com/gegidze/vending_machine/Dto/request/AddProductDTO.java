package com.gegidze.vending_machine.Dto.request;

import com.gegidze.vending_machine.Dto.IFrom;
import com.gegidze.vending_machine.repository.entity.Product;

/**
 * @Author: s.shakeri
 * at 6/5/2022
 **/

public class AddProductDTO implements IFrom<Product> {

    private String username;
    private String productName;
    private Long cost;
    private Long count;

    public String getUsername() {
        return username;
    }

    public AddProductDTO setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getProductName() {
        return productName;
    }

    public AddProductDTO setProductName(String productName) {
        this.productName = productName;
        return this;
    }

    public Long getCost() {
        return cost;
    }

    public AddProductDTO setCost(Long cost) {
        this.cost = cost;
        return this;
    }

    public Long getCount() {
        return count;
    }

    public AddProductDTO setCount(Long count) {
        this.count = count;
        return this;
    }

    @Override
    public AddProductDTO from(Product source) {
        return this
                .setCost(source.getCost())
                .setCount(source.getAvailableAmount())
                .setProductName(source.getProductName())
                .setUsername(source.getUser().getUsername());
    }
}
