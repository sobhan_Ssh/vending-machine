package com.gegidze.vending_machine.Dto.request;

/**
 * @Author: s.shakeri
 * at 6/5/2022
 **/

public class DepositDTO {

    private String username;
    private Long depositAmount;

    public String getUsername() {
        return username;
    }

    public DepositDTO setUsername(String username) {
        this.username = username;
        return this;
    }

    public Long getDepositAmount() {
        return depositAmount;
    }

    public DepositDTO setDepositAmount(Long depositAmount) {
        this.depositAmount = depositAmount;
        return this;
    }
}
