package com.gegidze.vending_machine.Dto.request;

/**
 * @Author: s.shakeri
 * at 6/6/2022
 **/

public class BuyDTO {

    private String username;
    private Long productId;
    private Long amount;

    public String getUsername() {
        return username;
    }

    public BuyDTO setUsername(String username) {
        this.username = username;
        return this;
    }

    public Long getProductId() {
        return productId;
    }

    public BuyDTO setProductId(Long productId) {
        this.productId = productId;
        return this;
    }

    public Long getAmount() {
        return amount;
    }

    public BuyDTO setAmount(Long amount) {
        this.amount = amount;
        return this;
    }
}
