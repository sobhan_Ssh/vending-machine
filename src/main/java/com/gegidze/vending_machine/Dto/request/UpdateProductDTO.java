package com.gegidze.vending_machine.Dto.request;

import com.gegidze.vending_machine.Dto.IFrom;
import com.gegidze.vending_machine.repository.entity.Product;

/**
 * @Author: s.shakeri
 * at 6/5/2022
 **/

public class UpdateProductDTO implements IFrom<Product> {

    private String username;
    private Long productId;
    private String productName;
    private Long cost;
    private Long count;

    public String getUsername() {
        return username;
    }

    public UpdateProductDTO setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getProductName() {
        return productName;
    }

    public UpdateProductDTO setProductName(String productName) {
        this.productName = productName;
        return this;
    }

    public Long getCost() {
        return cost;
    }

    public UpdateProductDTO setCost(Long cost) {
        this.cost = cost;
        return this;
    }

    public Long getCount() {
        return count;
    }

    public UpdateProductDTO setCount(Long count) {
        this.count = count;
        return this;
    }

    public Long getProductId() {
        return productId;
    }

    public UpdateProductDTO setProductId(Long productId) {
        this.productId = productId;
        return this;
    }

    @Override
    public UpdateProductDTO from(Product source) {
        return this
                .setProductId(source.getId())
                .setCost(source.getCost())
                .setCount(source.getAvailableAmount())
                .setProductName(source.getProductName())
                .setUsername(source.getUser().getUsername());
    }
}
