package com.gegidze.vending_machine.Dto.request;


import com.gegidze.vending_machine.Dto.BasicUser;

public class BaseRequest {

    private final BasicUser user = new BasicUser();

    public BasicUser getUser() {
        return user;
    }

}
