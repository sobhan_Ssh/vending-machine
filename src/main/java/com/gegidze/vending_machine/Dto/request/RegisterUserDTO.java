package com.gegidze.vending_machine.Dto.request;

import com.gegidze.vending_machine.annotation.RegisterUserRequest;
import com.gegidze.vending_machine.annotation.group.AdvancedCheck;
import com.gegidze.vending_machine.annotation.group.GroupCheck;
import com.gegidze.vending_machine.enums.UserRole;
import com.gegidze.vending_machine.util.Constant;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @Author: s.shakeri
 * at 6/4/2022
 **/

@RegisterUserRequest(groups = GroupCheck.ClassCheck.class)
public class RegisterUserDTO extends BaseRequest {

    @NotEmpty(message = Constant.REGISTER_USER_USERNAME_EMPTY)
    @NotNull(message = Constant.REGISTER_USER_USERNAME_EMPTY)
    private String username;

    @NotEmpty(message = Constant.REGISTER_USER_PASSWORD_EMPTY)
    @NotNull(message = Constant.REGISTER_USER_PASSWORD_EMPTY)
    private String password;

    @NotNull(message = Constant.USER_ROLE_NULL)
    @com.gegidze.vending_machine.annotation.UserRole(groups = AdvancedCheck.class)
    private UserRole role;

    public String getUsername() {
        return username;
    }

    public RegisterUserDTO setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public RegisterUserDTO setPassword(String password) {
        this.password = password;
        return this;
    }

    public UserRole getRole() {
        return role;
    }

    public RegisterUserDTO setRole(UserRole role) {
        this.role = role;
        return this;
    }
}
