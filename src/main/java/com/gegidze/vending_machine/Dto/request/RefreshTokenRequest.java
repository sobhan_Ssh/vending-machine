package com.gegidze.vending_machine.Dto.request;

public class RefreshTokenRequest {

    private String token;

    public String getToken() {
        return token;
    }

    public RefreshTokenRequest setToken(String token) {
        this.token = token;
        return this;
    }
}
