package com.gegidze.vending_machine.Dto.request;

/**
 * @Author: s.shakeri
 * at 6/6/2022
 **/

public class ResetDepositDTO {

    private String username;

    public String getUsername() {
        return username;
    }

    public ResetDepositDTO setUsername(String username) {
        this.username = username;
        return this;
    }
}
