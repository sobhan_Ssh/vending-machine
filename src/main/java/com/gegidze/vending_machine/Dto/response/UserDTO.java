package com.gegidze.vending_machine.Dto.response;

import com.gegidze.vending_machine.Dto.IFrom;
import com.gegidze.vending_machine.enums.UserRole;
import com.gegidze.vending_machine.enums.UserStatus;
import com.gegidze.vending_machine.repository.entity.User;

/**
 * @Author: s.shakeri
 * at 6/4/2022
 **/

public class UserDTO implements IFrom<User> {

    private String username;
    private UserRole role;
    private UserStatus status;

    public String getUsername() {
        return username;
    }

    public UserDTO setUsername(String username) {
        this.username = username;
        return this;
    }

    public UserRole getRole() {
        return role;
    }

    public UserDTO setRole(UserRole role) {
        this.role = role;
        return this;
    }

    public UserStatus getStatus() {
        return status;
    }

    public UserDTO setStatus(UserStatus status) {
        this.status = status;
        return this;
    }

    @Override
    public UserDTO from(User source) {
        return this
                .setRole(source.getRole())
                .setUsername(source.getUsername())
                .setStatus(source.getStatus());
    }
}
