package com.gegidze.vending_machine.Dto.response;

import com.gegidze.vending_machine.Dto.IFrom;
import com.gegidze.vending_machine.repository.entity.Deposit;

/**
 * @Author: s.shakeri
 * at 6/5/2022
 **/

public class UserDepositDTO implements IFrom<Deposit> {

    private String username;
    private Long deposit;
    private Long total;

    public String getUsername() {
        return username;
    }

    public UserDepositDTO setUsername(String username) {
        this.username = username;
        return this;
    }

    public Long getDeposit() {
        return deposit;
    }

    public UserDepositDTO setDeposit(Long deposit) {
        this.deposit = deposit;
        return this;
    }

    public Long getTotal() {
        return total;
    }

    public UserDepositDTO setTotal(Long total) {
        this.total = total;
        return this;
    }

    @Override
    public UserDepositDTO from(Deposit source) {
        return this
                .setDeposit(source.getDepositAmount())
                .setTotal(source.getTotalAmount())
                .setUsername(source.getUser().getUsername());
    }
}
