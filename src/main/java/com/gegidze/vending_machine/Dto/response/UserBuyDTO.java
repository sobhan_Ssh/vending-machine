package com.gegidze.vending_machine.Dto.response;

import com.gegidze.vending_machine.Dto.IFrom;
import com.gegidze.vending_machine.repository.entity.Buy;

/**
 * @Author: s.shakeri
 * at 6/6/2022
 **/

public class UserBuyDTO implements IFrom<Buy> {

    private String username;
    private Long productId;
    private Long amount;

    public String getUsername() {
        return username;
    }

    public UserBuyDTO setUsername(String username) {
        this.username = username;
        return this;
    }

    public Long getProductId() {
        return productId;
    }

    public UserBuyDTO setProductId(Long productId) {
        this.productId = productId;
        return this;
    }

    public Long getAmount() {
        return amount;
    }

    public UserBuyDTO setAmount(Long amount) {
        this.amount = amount;
        return this;
    }

    @Override
    public UserBuyDTO from(Buy source) {
        return this
                .setUsername(source.getUser().getUsername())
                .setProductId(source.getProduct().getId())
                .setAmount(source.getAmount());
    }
}
