package com.gegidze.vending_machine.Dto.response;

import com.gegidze.vending_machine.Dto.IFrom;
import com.gegidze.vending_machine.enums.ProductStatus;
import com.gegidze.vending_machine.repository.entity.Product;

/**
 * @Author: s.shakeri
 * at 6/5/2022
 **/

public class ProductDTO implements IFrom<Product> {

    private Long productId;
    private String productName;
    private Long cost;
    private Long count;
    private ProductStatus status;

    public Long getProductId() {
        return productId;
    }

    public ProductDTO setProductId(Long productId) {
        this.productId = productId;
        return this;
    }

    public String getProductName() {
        return productName;
    }

    public ProductDTO setProductName(String productName) {
        this.productName = productName;
        return this;
    }

    public Long getCost() {
        return cost;
    }

    public ProductDTO setCost(Long cost) {
        this.cost = cost;
        return this;
    }

    public Long getCount() {
        return count;
    }

    public ProductDTO setCount(Long count) {
        this.count = count;
        return this;
    }

    public ProductStatus getStatus() {
        return status;
    }

    public ProductDTO setStatus(ProductStatus status) {
        this.status = status;
        return this;
    }

    @Override
    public ProductDTO from(Product source) {
        return this
                .setProductId(source.getId())
                .setCost(source.getCost())
                .setCount(source.getAvailableAmount())
                .setProductName(source.getProductName())
                .setStatus(source.getStatus());
    }
}
