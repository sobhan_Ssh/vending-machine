package com.gegidze.vending_machine.Dto.response;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class TokenResponse {
    private String token;
    private String refresh;
    @JsonIgnore
    private String refreshCookie;

    public String getToken() {
        return token;
    }

    public TokenResponse setToken(String token) {
        this.token = token;
        return this;
    }

    public String getRefresh() {
        return refresh;
    }

    public TokenResponse setRefresh(String refresh) {
        this.refresh = refresh;
        return this;
    }

    public String getRefreshCookie() {
        return refreshCookie;
    }

    public TokenResponse setRefreshCookie(String refreshCookie) {
        this.refreshCookie = refreshCookie;
        return this;
    }
}
