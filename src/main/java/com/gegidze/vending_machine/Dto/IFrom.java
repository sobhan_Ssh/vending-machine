package com.gegidze.vending_machine.Dto;

public interface IFrom<SourceType> {

    IFrom<SourceType> from(SourceType source);

}
