package com.gegidze.vending_machine.Dto;

public interface IInto<DestinationType> {

    DestinationType into(DestinationType destination);

}
