package com.gegidze.vending_machine.Dto;

import com.gegidze.vending_machine.repository.entity.User;

public class BasicUser implements IFrom<User> {

    private String username;

    public String getUsername() {
        return username;
    }

    public BasicUser setUsername(String username) {
        this.username = username;
        return this;
    }

    @Override
    public BasicUser from(User source) {
        return this
                .setUsername(source.getUsername());
    }
}
