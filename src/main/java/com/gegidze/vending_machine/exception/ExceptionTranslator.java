package com.gegidze.vending_machine.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.lang.Nullable;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@ControllerAdvice
public class ExceptionTranslator {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionTranslator.class);

    private HttpHeaders getHeaders(WebRequest request) {
        HttpHeaders httpHeaders = new HttpHeaders();
        List<String> vary = new ArrayList<>();
        vary.add("Origin");
        vary.add("Access-Control-Request-Method");
        vary.add("Access-Control-Request-Headers");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        //httpHeaders.setVary(vary);
        return httpHeaders;
    }

    @ExceptionHandler(value
            = {BaseException.class})
    protected ResponseEntity<Object> handleBaseException(
            BaseException ex, WebRequest request) throws IOException {

        // TODO Set errorMessage with statusCode
//        if (isNullOrEmpty(ex.errorMessage)) {
//            ex.setErrorMessage(i18n.t(Constant.STATUS_CODE(ex.statusCode.value())));
//        } else {
//            ex.setErrorMessage(i18n.t(ex.errorMessage));
//        }

        return handleExceptionInternal(ex, ex,
                getHeaders(request), ex.getStatusCode(), request);
    }

    @ExceptionHandler(value
            = {ConstraintViolationException.class})
    protected ResponseEntity<Object> handleConstraintViolationException(
            ConstraintViolationException ex, WebRequest request) throws IOException {
        BaseException res = new BadRequestException();
        for (ConstraintViolation cv : ex.getConstraintViolations()) {
            res.getDetail().add(
                    new BaseException.ErrorDetail()
                            .setType(cv.getMessage())
                            .setLoc(cv.getPropertyPath().toString())
            );
        }
        return handleExceptionInternal(ex, res,
                getHeaders(request), res.getStatusCode(), request);
    }

    @ExceptionHandler(value
            = {MethodArgumentNotValidException.class})
    protected ResponseEntity<Object> handleMethodArgumentNotValidException(
            MethodArgumentNotValidException ex, WebRequest request) throws IOException {

        BaseException res = new BadRequestException();


        List<FieldError> fieldErrors = ex.getBindingResult().getFieldErrors();
        List<ObjectError> globalErrors = ex.getBindingResult().getGlobalErrors();
        List<BaseException.ErrorDetail> errorDetails = new ArrayList<>(fieldErrors.size() + globalErrors.size());

        for (ObjectError error : globalErrors) {
            errorDetails.add(
                    new BaseException.ErrorDetail()
                            .setLoc(error.getObjectName())
                            .setType(error.getDefaultMessage())
            );
        }

        for (FieldError error : fieldErrors) {
            errorDetails.add(
                    new BaseException.ErrorDetail()
                            .setLoc(error.getField())
                            .setType(error.getDefaultMessage())
            );
        }

        res.getDetail().addAll(errorDetails);

        return handleExceptionInternal(ex, res,
                getHeaders(request), res.getStatusCode(), request);
    }

    @ExceptionHandler(value
            = {HttpMessageNotReadableException.class})
    protected ResponseEntity<Object> handleHttpMessageNotReadableException(HttpMessageNotReadableException ex, WebRequest request) {


        BaseException res = new BadRequestException();

        return handleExceptionInternal(ex, res,
                getHeaders(request), res.getStatusCode(), request);

    }

    @ExceptionHandler(value
            = {RuntimeException.class})
    protected ResponseEntity<Object> handleRuntimeException(
            RuntimeException ex, WebRequest request) throws IOException {
        BaseException res = new BaseException();
        res.setStackTrace(ex.getStackTrace());
        LOGGER.error(ex.getMessage(), ex);
        return handleExceptionInternal(ex, res,
                new HttpHeaders(), res.getStatusCode(), request);
    }

    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, @Nullable Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
        /*if (HttpStatus.INTERNAL_SERVER_ERROR.equals(status)) {
            request.setAttribute("javax.servlet.error.exception", ex, 0);
        }*/

        return new ResponseEntity<>(body, headers, status);
    }

}
