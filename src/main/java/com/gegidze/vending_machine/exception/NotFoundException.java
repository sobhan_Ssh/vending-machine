package com.gegidze.vending_machine.exception;

import org.springframework.http.HttpStatus;

public class NotFoundException extends BaseException {

    public NotFoundException() {
        super.statusCode = HttpStatus.NOT_FOUND;
    }

    public NotFoundException(ErrorDetail username) {

    }
}
