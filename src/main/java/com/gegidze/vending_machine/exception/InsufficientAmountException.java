package com.gegidze.vending_machine.exception;

import org.springframework.http.HttpStatus;

public class InsufficientAmountException extends BaseException {

    public InsufficientAmountException() {
        super.statusCode = HttpStatus.CONFLICT;
    }

    public InsufficientAmountException(BaseException.ErrorDetail username) {

    }
}
