package com.gegidze.vending_machine.exception;

import org.springframework.http.HttpStatus;


public class BadRequestException extends BaseException {
    public BadRequestException() {
        super(HttpStatus.BAD_REQUEST);
    }

    public BadRequestException(ErrorDetail errorDetail) {
        this();
        super.getDetail().add(errorDetail);
    }

}
