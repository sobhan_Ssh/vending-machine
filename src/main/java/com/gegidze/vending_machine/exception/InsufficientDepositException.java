package com.gegidze.vending_machine.exception;

import org.springframework.http.HttpStatus;

public class InsufficientDepositException extends BaseException {

    public InsufficientDepositException() {
        super.statusCode = HttpStatus.PAYMENT_REQUIRED;
    }

    public InsufficientDepositException(BaseException.ErrorDetail username) {

    }
}
