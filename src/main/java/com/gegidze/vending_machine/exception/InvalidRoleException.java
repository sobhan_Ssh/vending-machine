package com.gegidze.vending_machine.exception;

import org.springframework.http.HttpStatus;

public class InvalidRoleException extends BaseException {

    public InvalidRoleException() {
        super.statusCode = HttpStatus.CONFLICT;
    }

    public InvalidRoleException(ErrorDetail username) {

    }
}
