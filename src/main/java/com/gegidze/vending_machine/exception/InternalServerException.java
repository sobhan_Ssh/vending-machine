package com.gegidze.vending_machine.exception;

import org.springframework.http.HttpStatus;

public class InternalServerException extends BaseException {
    public InternalServerException() {
        super(HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
