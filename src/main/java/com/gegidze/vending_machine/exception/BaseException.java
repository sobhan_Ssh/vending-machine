package com.gegidze.vending_machine.exception;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;


@JsonIgnoreProperties({"stackTrace", "suppressed"})
@JsonInclude(JsonInclude.Include.CUSTOM)
public class BaseException extends RuntimeException {

    public BaseException(HttpStatus statusCode) {
        this.statusCode = statusCode;
    }

    public BaseException() {
        this(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @JsonIgnore
    protected HttpStatus statusCode;

    @JsonInclude
    private List<ErrorDetail> detail = new ArrayList<>();

    public HttpStatus getStatusCode() {
        return statusCode;
    }

    public void setDetail(List<ErrorDetail> detail) {
        this.detail = detail;
    }

    public BaseException addDetail(ErrorDetail detail) {
        this.getDetail().add(detail);
        return this;
    }

    public List<ErrorDetail> getDetail() {
        return detail;
    }

    public static class ErrorDetail {
        private String loc;
        private String type;

        public String getLoc() {
            return loc;
        }

        public ErrorDetail setLoc(String loc) {
            this.loc = loc;
            return this;
        }

        public String getType() {
            return type;
        }

        public ErrorDetail setType(String type) {
            this.type = type;
            return this;
        }
    }
}
