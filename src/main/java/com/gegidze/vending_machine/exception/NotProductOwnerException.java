package com.gegidze.vending_machine.exception;

import org.springframework.http.HttpStatus;

public class NotProductOwnerException extends BaseException {

    public NotProductOwnerException() {
        super.statusCode = HttpStatus.FORBIDDEN;
    }

    public NotProductOwnerException(BaseException.ErrorDetail product) {

    }
}
