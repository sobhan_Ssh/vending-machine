package com.gegidze.vending_machine.exception;

import org.springframework.http.HttpStatus;

public class DepositAmountNotMultipleOfFiveException extends BaseException {

    public DepositAmountNotMultipleOfFiveException() {
        super.statusCode = HttpStatus.CONFLICT;
    }

    public DepositAmountNotMultipleOfFiveException(BaseException.ErrorDetail username) {

    }
}
