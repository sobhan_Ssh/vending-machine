package com.gegidze.vending_machine.controller;

import com.gegidze.vending_machine.Dto.request.BaseRequest;
import com.gegidze.vending_machine.Dto.request.LoginRequest;
import com.gegidze.vending_machine.Dto.request.RefreshTokenRequest;
import com.gegidze.vending_machine.Dto.response.TokenResponse;
import com.gegidze.vending_machine.annotation.AllowAnonymous;
import com.gegidze.vending_machine.service.IJwtService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseCookie;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@RestController
public class JwtController {

    final static String refreshCookie = "refresh";
    private final IJwtService service;

    public JwtController(IJwtService service) {
        this.service = service;
    }

    @AllowAnonymous
    @RequestMapping(value = "/jwt", method = RequestMethod.POST)
    public TokenResponse token(
            @RequestHeader Map<String, String> requestHeaders,
            @RequestBody LoginRequest clientRequest,
            @RequestParam(defaultValue = "Anonymous", required = false) String client,
            HttpServletResponse servletResponse,
            HttpServletRequest request) {
        TokenResponse token = service.generate(clientRequest);

        //update cookies
        ResponseCookie refresh = ResponseCookie
                .from(refreshCookie, token.getRefreshCookie())
                .httpOnly(true)
                .sameSite("None")
                .secure(true)
                .maxAge(600L)
                .build();
        servletResponse.addHeader(HttpHeaders.SET_COOKIE, refresh.toString());
        return token;
    }

    @AllowAnonymous
    @RequestMapping(value = "/jwt", method = RequestMethod.PUT)
    public TokenResponse refresh(
            @RequestHeader Map<String, String> requestHeaders,
            @RequestBody RefreshTokenRequest clientRequest,
            @CookieValue(refreshCookie) String cookie,
            HttpServletResponse servletResponse) {
        TokenResponse token = service.refresh(cookie, clientRequest);
        //update cookies
        ResponseCookie refresh = ResponseCookie
                .from(refreshCookie, token.getRefreshCookie())
                .httpOnly(true)
                .sameSite("None")
                .secure(true)
                .maxAge(600L)
                .build();
        servletResponse.addHeader(HttpHeaders.SET_COOKIE, refresh.toString());
        return token;
    }

    @RequestMapping(value = "/jwt", method = RequestMethod.DELETE)
    public Object logout(
            @RequestHeader Map<String, String> requestHeaders,
            HttpServletResponse servletResponse,
            BaseRequest baseRequest) {
        Cookie refresh = new Cookie(refreshCookie, null);
        refresh.setHttpOnly(true);
        //TODO Is panel ssl protected?
        refresh.setSecure(true);
        refresh.setMaxAge(0);
        servletResponse.addCookie(refresh);
        servletResponse.setStatus(HttpStatus.NO_CONTENT.value());
        return null;
    }
}
