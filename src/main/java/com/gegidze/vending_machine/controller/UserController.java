package com.gegidze.vending_machine.controller;

import com.gegidze.vending_machine.Dto.request.RegisterUserDTO;
import com.gegidze.vending_machine.Dto.request.UpdateUserDTO;
import com.gegidze.vending_machine.Dto.response.UserDTO;
import com.gegidze.vending_machine.annotation.AllowAnonymous;
import com.gegidze.vending_machine.annotation.group.GroupCheck;
import com.gegidze.vending_machine.service.IUserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @Author: s.shakeri
 * at 6/4/2022
 **/

@Validated
@RestController
@RequestMapping(path = "/user")
public class UserController {

    private IUserService service;

    public UserController(IUserService service) {
        this.service = service;
    }

    @AllowAnonymous
    @PostMapping
    public ResponseEntity<UserDTO> registerUser(@RequestHeader Map<String, String> requestHeaders,
                                                @Validated(GroupCheck.OrderedCheck.class)
                                                @RequestBody RegisterUserDTO userDTO) {
        return new ResponseEntity<>(service.registerUser(userDTO), HttpStatus.CREATED);
    }

    @GetMapping(value = "/{username}")
    public ResponseEntity<UserDTO> getUser(@RequestHeader Map<String, String> requestHeaders,
                                           @PathVariable String username) {
        return new ResponseEntity<>(service.findUserByUsername(username), HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<UserDTO> updateUser(@RequestHeader Map<String, String> requestHeaders,
                                              @Validated(GroupCheck.OrderedCheck.class)
                                              @RequestBody UpdateUserDTO userDTO) {
        return new ResponseEntity<>(service.updateUser(userDTO), HttpStatus.OK);
    }


    @DeleteMapping(value = "/{username}")
    public ResponseEntity<UserDTO> deleteUser(@RequestHeader Map<String, String> requestHeaders,
                                              @PathVariable String username) {
        return new ResponseEntity<>(service.deleteUserByUsername(username), HttpStatus.OK);
    }


}
