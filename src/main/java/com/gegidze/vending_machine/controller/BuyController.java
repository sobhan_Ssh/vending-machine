package com.gegidze.vending_machine.controller;

import com.gegidze.vending_machine.Dto.request.BuyDTO;
import com.gegidze.vending_machine.Dto.response.UserBuyDTO;
import com.gegidze.vending_machine.service.IBuyService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @Author: s.shakeri
 * at 6/4/2022
 **/

@RestController
@RequestMapping(path = "/buy")
public class BuyController {

    private final IBuyService service;

    public BuyController(IBuyService service) {
        this.service = service;
    }

    @PostMapping
    public ResponseEntity<UserBuyDTO> buy(@RequestHeader Map<String, String> requestHeaders,
                                          @RequestBody BuyDTO buyDTO) {
        return new ResponseEntity<>(service.buyProductById(buyDTO), HttpStatus.OK);
    }

}
