package com.gegidze.vending_machine.controller;

import com.gegidze.vending_machine.Dto.request.DepositDTO;
import com.gegidze.vending_machine.Dto.request.ResetDepositDTO;
import com.gegidze.vending_machine.Dto.response.UserDepositDTO;
import com.gegidze.vending_machine.service.IDepositService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @Author: s.shakeri
 * at 6/4/2022
 **/

@RestController
@RequestMapping(path = "/deposit")
public class DepositController {

    private final IDepositService service;

    public DepositController(IDepositService service) {
        this.service = service;
    }

    @PostMapping
    public ResponseEntity<UserDepositDTO> deposit(@RequestHeader Map<String, String> requestHeaders,
                                                  @RequestBody DepositDTO depositDTO) {
        return new ResponseEntity<>(service.depositByUsername(depositDTO), HttpStatus.OK);
    }

    @PutMapping("/reset")
    public ResponseEntity<UserDepositDTO> reset(@RequestHeader Map<String, String> requestHeaders,
                                                @RequestBody ResetDepositDTO resetDepositDTO) {
        return new ResponseEntity<>(service.resetDeposit(resetDepositDTO), HttpStatus.OK);
    }
}
