package com.gegidze.vending_machine.controller;

import com.gegidze.vending_machine.Dto.request.AddProductDTO;
import com.gegidze.vending_machine.Dto.request.UpdateProductDTO;
import com.gegidze.vending_machine.Dto.response.ProductDTO;
import com.gegidze.vending_machine.service.IProductService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @Author: s.shakeri
 * at 6/4/2022
 **/

@RestController
@RequestMapping(path = "/product")
public class ProductController {

    private final IProductService service;

    public ProductController(IProductService service) {
        this.service = service;
    }

    @PostMapping
    public ResponseEntity<ProductDTO> addProduct(@RequestHeader Map<String, String> requestHeaders,
                                                 @RequestBody AddProductDTO productDTO) {
        return new ResponseEntity<>(service.addProduct(productDTO), HttpStatus.OK);
    }

    @GetMapping("/list")
    public ResponseEntity<List<ProductDTO>> getProductList(@RequestHeader Map<String, String> requestHeaders,
                                                           @RequestParam int pageNum,
                                                           @RequestParam int pageSize) {
        return new ResponseEntity<>(service.getProductList(pageNum, pageSize), HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<ProductDTO> updateProduct(@RequestHeader Map<String, String> requestHeaders,
                                                    @RequestBody UpdateProductDTO productDTO) {
        return new ResponseEntity<>(service.updateProduct(productDTO), HttpStatus.OK);
    }

    @DeleteMapping("/{username}/{productId}")
    public ResponseEntity<ProductDTO> deleteProduct(@RequestHeader Map<String, String> requestHeaders,
                                                    @PathVariable String username,
                                                    @PathVariable Long productId) {
        return new ResponseEntity<>(service.deleteByProductName(username, productId), HttpStatus.OK);

    }
}
