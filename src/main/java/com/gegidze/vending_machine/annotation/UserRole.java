package com.gegidze.vending_machine.annotation;

import com.gegidze.vending_machine.annotation.validator.UserRoleValidator;
import com.gegidze.vending_machine.util.Constant;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author S.shakery
 */

@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RUNTIME)
@Constraint(validatedBy = UserRoleValidator.class)
public @interface UserRole {
    String message() default Constant.USER_ROLE_INVALID;
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
