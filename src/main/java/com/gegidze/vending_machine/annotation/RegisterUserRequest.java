package com.gegidze.vending_machine.annotation;

import com.gegidze.vending_machine.annotation.validator.RegisterUserRequestValidator;
import com.gegidze.vending_machine.util.Constant;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = RegisterUserRequestValidator.class)
public @interface RegisterUserRequest {

    String message() default Constant.REGISTER_USER_REQUEST_INVALID;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
