package com.gegidze.vending_machine.annotation.validator;


import com.gegidze.vending_machine.annotation.UserRole;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author S.shakery
 */

public class UserRoleValidator implements ConstraintValidator<UserRole, com.gegidze.vending_machine.enums.UserRole>
{
    @Override
    public void initialize(UserRole constraintAnnotation) {
    }

    @Override
    public boolean isValid(com.gegidze.vending_machine.enums.UserRole role, ConstraintValidatorContext context) {
        // Check side is valid in IssueReason
        return role != null && com.gegidze.vending_machine.enums.UserRole.contains(role.getVal());
    }
}
