package com.gegidze.vending_machine.annotation.validator;

import com.gegidze.vending_machine.Dto.request.RegisterUserDTO;
import com.gegidze.vending_machine.annotation.RegisterUserRequest;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

public class RegisterUserRequestValidator implements ConstraintValidator<RegisterUserRequest, RegisterUserDTO> {

    @Override
    public void initialize(RegisterUserRequest constraintAnnotation) {

    }

    @Override
    public boolean isValid(RegisterUserDTO requestDTO, ConstraintValidatorContext context) {
        return Objects.nonNull(requestDTO);
    }
}
