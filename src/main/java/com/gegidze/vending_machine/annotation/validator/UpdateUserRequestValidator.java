package com.gegidze.vending_machine.annotation.validator;

import com.gegidze.vending_machine.Dto.request.UpdateUserDTO;
import com.gegidze.vending_machine.annotation.UpdateUserRequest;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

public class UpdateUserRequestValidator implements ConstraintValidator<UpdateUserRequest, UpdateUserDTO> {

    @Override
    public void initialize(UpdateUserRequest constraintAnnotation) {

    }

    @Override
    public boolean isValid(UpdateUserDTO requestDTO, ConstraintValidatorContext context) {
        return Objects.nonNull(requestDTO);
    }
}
