package com.gegidze.vending_machine.service;

import com.gegidze.vending_machine.Dto.request.AddProductDTO;
import com.gegidze.vending_machine.Dto.request.UpdateProductDTO;
import com.gegidze.vending_machine.Dto.response.ProductDTO;

import java.util.List;

/**
 * @Author: s.shakeri
 * at 6/4/2022
 **/

public interface IProductService {

    ProductDTO addProduct(AddProductDTO productDTO);

    List<ProductDTO> getProductList(int pageNum, int pageSize);

    ProductDTO updateProduct(UpdateProductDTO productDTO);

    ProductDTO deleteByProductName(String username, Long productId);
}
