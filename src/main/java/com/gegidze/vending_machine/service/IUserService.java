package com.gegidze.vending_machine.service;

import com.gegidze.vending_machine.Dto.request.RegisterUserDTO;
import com.gegidze.vending_machine.Dto.request.UpdateUserDTO;
import com.gegidze.vending_machine.Dto.response.UserDTO;

/**
 * @Author: s.shakeri
 * at 6/4/2022
 **/

public interface IUserService {

    UserDTO registerUser(RegisterUserDTO userDTO);

    UserDTO findUserByUsername(String username);

    UserDTO updateUser(UpdateUserDTO userDTO);

    UserDTO deleteUserByUsername(String username);
}
