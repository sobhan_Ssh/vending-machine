package com.gegidze.vending_machine.service;

import com.gegidze.vending_machine.Dto.request.LoginRequest;
import com.gegidze.vending_machine.Dto.request.RefreshTokenRequest;
import com.gegidze.vending_machine.Dto.response.TokenResponse;

public interface IJwtService {

    TokenResponse generate(LoginRequest clientRequest);

    TokenResponse refresh(String cookie, RefreshTokenRequest clientRequest);
}
