package com.gegidze.vending_machine.service;

import com.gegidze.vending_machine.Dto.request.BuyDTO;
import com.gegidze.vending_machine.Dto.response.UserBuyDTO;

/**
 * @Author: s.shakeri
 * at 6/4/2022
 **/

public interface IBuyService {

    UserBuyDTO buyProductById(BuyDTO buyDTO);
}
