package com.gegidze.vending_machine.service.impl;

import com.gegidze.vending_machine.Dto.request.AddProductDTO;
import com.gegidze.vending_machine.Dto.request.UpdateProductDTO;
import com.gegidze.vending_machine.Dto.response.ProductDTO;
import com.gegidze.vending_machine.enums.ProductStatus;
import com.gegidze.vending_machine.enums.UserRole;
import com.gegidze.vending_machine.exception.BaseException;
import com.gegidze.vending_machine.exception.InvalidRoleException;
import com.gegidze.vending_machine.exception.NotFoundException;
import com.gegidze.vending_machine.exception.NotProductOwnerException;
import com.gegidze.vending_machine.repository.ProductRepository;
import com.gegidze.vending_machine.repository.UserRepository;
import com.gegidze.vending_machine.repository.entity.Product;
import com.gegidze.vending_machine.service.IProductService;
import com.gegidze.vending_machine.util.Constant;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @Author: s.shakeri
 * at 6/4/2022
 **/

@Service
public class ProductServiceImpl implements IProductService {

    private final ProductRepository repository;
    private final UserRepository userRepository;

    public ProductServiceImpl(ProductRepository repository, UserRepository userRepository) {
        this.repository = repository;
        this.userRepository = userRepository;
    }

    @Override
    public ProductDTO addProduct(AddProductDTO productDTO) {
        Product product = new Product();
        var temp = userRepository.findByUsername(productDTO.getUsername());
        if (temp.isPresent()) {
            if (Objects.equals(temp.get().getRole(), UserRole.SELLER)) {
                product = repository.save(product
                        .setAvailableAmount(productDTO.getCount())
                        .setCost(productDTO.getCost())
                        .setProductName(productDTO.getProductName())
                        .setUser(temp.get())
                        .setStatus(ProductStatus.ACTIVE)
                );
                return new ProductDTO().from(product);
            } else
                throw new InvalidRoleException(new BaseException.ErrorDetail()
                        .setLoc("userRole")
                        .setType(Constant.SELLER_USER_ROLE_REQUIRED));
        } else
            throw new NotFoundException(new BaseException.ErrorDetail()
                    .setLoc("username")
                    .setType(Constant.USER_USERNAME_NOT_FOUND));
    }

    @Override
    public List<ProductDTO> getProductList(int pageNum, int pageSize) {
        List<ProductDTO> productDTOS = new ArrayList<>();
        var temp = repository.findAll(PageRequest.of(pageNum, pageSize));
        if (!temp.isEmpty()) {
            for (Product product : temp.getContent())
                productDTOS.add(new ProductDTO().from(product));
            return productDTOS;
        } else
            throw new NotFoundException(new BaseException.ErrorDetail()
                    .setLoc("username")
                    .setType(Constant.PRODUCT_NOT_FOUND));
    }

    @Override
    public ProductDTO updateProduct(UpdateProductDTO productDTO) {
        var usr = userRepository.findByUsername(productDTO.getUsername());
        if (usr.isPresent()) {
            var temp = repository.findById(productDTO.getProductId());
            if (temp.isPresent()) {
                if (Objects.equals(temp.get().getUser().getId(), usr.get().getId())) {
                    Product product = temp.get();
                    product = repository.save(product
                            .setAvailableAmount(productDTO.getCount())
                            .setCost(productDTO.getCost())
                            .setProductName(productDTO.getProductName()));
                    return new ProductDTO().from(product);
                } else
                    throw new NotProductOwnerException(new BaseException.ErrorDetail()
                            .setLoc("user")
                            .setType(Constant.INVALID_PRODUCT_OWNER));
            } else
                throw new NotFoundException(new BaseException.ErrorDetail()
                        .setLoc("product")
                        .setType(Constant.PRODUCT_NOT_FOUND));
        } else
            throw new NotFoundException(new BaseException.ErrorDetail()
                    .setLoc("username")
                    .setType(Constant.USER_USERNAME_NOT_FOUND));
    }
    @Override
    public ProductDTO deleteByProductName(String username, Long productId) {
        var usr = userRepository.findByUsername(username);
        if (usr.isPresent()) {
            var temp = repository.findById(productId);
            if (temp.isPresent()) {
                if (Objects.equals(temp.get().getUser().getId(), usr.get().getId())) {
                    Product product = temp.get();
                    product = repository.save(product
                            .setStatus(ProductStatus.INACTIVE));
                    return new ProductDTO().from(product);
                } else
                    throw new NotProductOwnerException(new BaseException.ErrorDetail()
                            .setLoc("user")
                            .setType(Constant.INVALID_PRODUCT_OWNER));
            } else
                throw new NotFoundException(new BaseException.ErrorDetail()
                        .setLoc("product")
                        .setType(Constant.PRODUCT_NOT_FOUND));
        } else
            throw new NotFoundException(new BaseException.ErrorDetail()
                    .setLoc("username")
                    .setType(Constant.USER_USERNAME_NOT_FOUND));
    }
}
