package com.gegidze.vending_machine.service.impl;

import com.gegidze.vending_machine.Dto.request.RegisterUserDTO;
import com.gegidze.vending_machine.Dto.request.UpdateUserDTO;
import com.gegidze.vending_machine.Dto.response.UserDTO;
import com.gegidze.vending_machine.enums.UserStatus;
import com.gegidze.vending_machine.exception.BadRequestException;
import com.gegidze.vending_machine.exception.BaseException;
import com.gegidze.vending_machine.exception.NotFoundException;
import com.gegidze.vending_machine.repository.UserRepository;
import com.gegidze.vending_machine.repository.entity.User;
import com.gegidze.vending_machine.service.IUserService;
import com.gegidze.vending_machine.util.Constant;
import com.gegidze.vending_machine.util.HashUtils;
import org.springframework.stereotype.Service;

import java.util.Base64;
import java.util.Optional;
import java.util.Random;

/**
 * @Author: s.shakeri
 * at 6/4/2022
 **/

@Service
public class IUserServiceImpl implements IUserService {

    private UserRepository userRepository;

    public IUserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDTO registerUser(RegisterUserDTO userDTO) {
        User user = new User();
        byte[] salt = new byte[16];
        new Random().nextBytes(salt);
        var temp = userRepository.findByUsername(userDTO.getUsername());
        if (temp.isEmpty()) {
            return new UserDTO().from(userRepository.save(user
                    .setUsername(userDTO.getUsername())
                    .setRole(userDTO.getRole())
                    .setStatus(UserStatus.ACTIVE)
                    .setPassword(Base64.getEncoder().encodeToString(HashUtils.generatePasswordHash(salt, userDTO.getPassword())))
                    .setSalt(Base64.getEncoder().encodeToString(salt))));
        } else
            throw new BadRequestException(new BaseException.ErrorDetail()
                    .setLoc("username")
                    .setType(Constant.USER_USERNAME_DUPLICATE));

    }

    @Override
    public UserDTO findUserByUsername(String username) {
        Optional<User> user = userRepository.findByUsername(username);
        if (user.isPresent())
            return user.map(value -> new UserDTO().from(value)).orElse(new UserDTO());
        else
            throw new NotFoundException(new BaseException.ErrorDetail()
                    .setLoc("username")
                    .setType(Constant.USER_USERNAME_NOT_FOUND));
    }

    @Override
    public UserDTO updateUser(UpdateUserDTO userDTO) {
        Optional<User> tempUser = userRepository.findByUsername(userDTO.getUsername());
        if (tempUser.isPresent()) {
            User user = tempUser.get();
            user.setUsername(tempUser.get().getUsername());
            user.setPassword(Base64.getEncoder().encodeToString(HashUtils.generatePasswordHash(user.getSalt().getBytes(), userDTO.getPassword())));
            user.setRole(userDTO.getRole());
            return new UserDTO().from(userRepository.save(user));
        } else
            throw new NotFoundException(new BaseException.ErrorDetail()
                    .setLoc("username")
                    .setType(Constant.USER_USERNAME_NOT_FOUND));
    }

    @Override
    public UserDTO deleteUserByUsername(String username) {
        Optional<User> tempUser = userRepository.findByUsername(username);
        if (tempUser.isPresent()) {
            User user = tempUser.get();
            user.setStatus(UserStatus.INACTIVE);
            return new UserDTO().from(userRepository.save(user));
        } else
            throw new NotFoundException(new BaseException.ErrorDetail()
                    .setLoc("username")
                    .setType(Constant.USER_USERNAME_NOT_FOUND));
    }
}
