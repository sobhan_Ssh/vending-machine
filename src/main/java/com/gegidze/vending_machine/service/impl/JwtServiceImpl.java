package com.gegidze.vending_machine.service.impl;

import com.gegidze.vending_machine.Dto.BasicUser;
import com.gegidze.vending_machine.Dto.request.LoginRequest;
import com.gegidze.vending_machine.Dto.request.RefreshTokenRequest;
import com.gegidze.vending_machine.Dto.response.TokenResponse;
import com.gegidze.vending_machine.config.SsoConfig;
import com.gegidze.vending_machine.enums.UserStatus;
import com.gegidze.vending_machine.exception.BaseException;
import com.gegidze.vending_machine.exception.InternalServerException;
import com.gegidze.vending_machine.exception.UnauthorizedException;
import com.gegidze.vending_machine.repository.UserRepository;
import com.gegidze.vending_machine.repository.entity.User;
import com.gegidze.vending_machine.service.IJwtService;
import com.gegidze.vending_machine.util.HashUtils;
import com.sun.istack.NotNull;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.MalformedClaimException;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.springframework.stereotype.Service;

import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.AbstractMap;
import java.util.Base64;
import java.util.Optional;

@Service
public class JwtServiceImpl implements IJwtService {

    private final SsoConfig ssoConfig;
    private final JwtConsumer jwtConsumer;
    private final UserRepository userRepository;

    public JwtServiceImpl(SsoConfig ssoConfig, JwtConsumer jwtConsumer, UserRepository userRepository) {
        this.ssoConfig = ssoConfig;
        this.jwtConsumer = jwtConsumer;
        this.userRepository = userRepository;
    }

    @Override
    public TokenResponse generate(LoginRequest clientRequest) {
        TokenResponse response = new TokenResponse();
        // Resolve Key
        AbstractMap.SimpleEntry<String, PrivateKey> key = keyResolver(ssoConfig);

        //Login
        Optional<User> user = userRepository.findByUsername(clientRequest.getUsername());
        if (!user.isPresent() || !user.get().getPassword()
                .equals(Base64.getEncoder().encodeToString(HashUtils.generatePasswordHash(Base64.getDecoder().decode(user.get().getSalt()), clientRequest.getPassword())))
                || (user.get().getStatus() == UserStatus.INACTIVE)) {
            throw new UnauthorizedException().addDetail(new BaseException.ErrorDetail()
                    .setLoc("user")
                    .setType("invalid")
            );
        }
        BasicUser cuser = new BasicUser().from(user.get());
        // Generate Token
        response.setToken(generateToken(cuser, key, ssoConfig));
        // Generate RefreshToken
        JwtClaims claims = new JwtClaims();
        claims.setSubject(cuser.getUsername());
        String[] refresh = generateRefresh(claims, key, ssoConfig);
        response.setRefresh(refresh[1]);
        response.setRefreshCookie(refresh[0]);
        return response;
    }

    @Override
    public TokenResponse refresh(String cookie, RefreshTokenRequest clientRequest) {
        String uname;
        //Check refresh
        JwtClaims claims = null;
        try {
            String token = cookie + "." + clientRequest.getToken();
            claims = jwtConsumer.processToClaims(token);
            if (!claims.getClaimValue("typ").equals("ref")) {
                throw new UnauthorizedException().addDetail(new BaseException.ErrorDetail()
                        .setLoc("token")
                        .setType("invalid")
                );
            }
            uname = claims.getSubject();
        } catch (InvalidJwtException | NullPointerException | MalformedClaimException ex) {
            throw new UnauthorizedException().addDetail(new BaseException.ErrorDetail()
                    .setLoc("token")
                    .setType("invalid")
            );
        }
        //Extract User
        BasicUser cuser;
        try {
            var temp = userRepository.findByUsername(uname);
            if (temp.isPresent()) {
                cuser = new BasicUser().from(temp.get());
                cuser.setUsername(claims.getSubject());
            } else
                throw new UnauthorizedException().addDetail(new BaseException.ErrorDetail()
                        .setLoc("token")
                        .setType("invalid.user")
                );
        } catch (Exception e) {
            throw new UnauthorizedException().addDetail(new BaseException.ErrorDetail()
                    .setLoc("token")
                    .setType("invalid.user")
            );
        }
        TokenResponse response = new TokenResponse();
        // Resolve Key
        var key = keyResolver(ssoConfig);

        response.setToken(generateToken(cuser, key, ssoConfig));

        // Generate Refresh
        String[] refresh = generateRefresh(claims, key, ssoConfig);
        response.setRefresh(refresh[1]);
        response.setRefreshCookie(refresh[0]);

        return response;
    }

    private String generateToken(
            BasicUser user,
            AbstractMap.SimpleEntry<String, PrivateKey> key,
            SsoConfig config) {

        JwtClaims claims = new JwtClaims();
        //set claims
        claims.setIssuer(config.getIssuer());
        claims.setAudience(config.getAudiences());
        claims.setExpirationTimeMinutesInTheFuture(config.getExpiration());
        claims.setGeneratedJwtId(); // a unique identifier for the token
        claims.setIssuedAtToNow();  // when the token was issued/created (now)
        claims.setNotBeforeMinutesInThePast(2); // time before which the token is not yet valid (2 minutes ago)
        claims.setSubject(user.getUsername()); // the subject/principal is whom the token is about
        claims.setClaim("typ", "tok");
        claims.setClaim("usr", user);

        JsonWebSignature jws = new JsonWebSignature();
        jws.setPayload(claims.toJson());
        jws.setKey(key.getValue());
        jws.setKeyIdHeaderValue(key.getKey());
        jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.RSA_USING_SHA256);
        try {
            return jws.getCompactSerialization();
        } catch (Exception ex) {
            throw new InternalServerException();
        }
    }

    private String[] generateRefresh(
            JwtClaims claims,
            AbstractMap.SimpleEntry<String, PrivateKey> key,
            SsoConfig config) {
        String[] res = new String[2];

        claims.setIssuer(config.getIssuer());
        claims.setAudience(config.getAudiences());
        claims.setExpirationTimeMinutesInTheFuture(config.getRefreshExpire());
        claims.setGeneratedJwtId(); // a unique identifier for the token
        claims.setIssuedAtToNow();  // when the token was issued/created (now)
        claims.setNotBeforeMinutesInThePast(2); // time before which the token is not yet valid (2 minutes ago)
        claims.setClaim("typ", "ref");

        JsonWebSignature jws = new JsonWebSignature();
        jws.setPayload(claims.toJson());
        jws.setKey(key.getValue());
        jws.setKeyIdHeaderValue(key.getKey());
        jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.RSA_USING_SHA256);
        try {
            String[] tmp = jws.getCompactSerialization().split("\\.");
            res[0] = tmp[0] + "." + tmp[1];
            res[1] = tmp[2];
            return res;
        } catch (Exception ex) {
            throw new InternalServerException();
        }
    }

    private AbstractMap.SimpleEntry<String, PrivateKey> keyResolver(@NotNull SsoConfig config) {
        try {
            String key = "default";
            KeyFactory kf = KeyFactory.getInstance("RSA");
            PrivateKey privateKey = kf.generatePrivate(
                    new PKCS8EncodedKeySpec(Base64.getDecoder().decode(config.getPrivateKey())));
            return new AbstractMap.SimpleEntry<String, PrivateKey>(key, privateKey);
        } catch (Exception ex) {
            throw new InternalServerException();
        }
    }
}
