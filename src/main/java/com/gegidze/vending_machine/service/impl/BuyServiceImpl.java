package com.gegidze.vending_machine.service.impl;

import com.gegidze.vending_machine.Dto.request.BuyDTO;
import com.gegidze.vending_machine.Dto.response.UserBuyDTO;
import com.gegidze.vending_machine.enums.DepositStatus;
import com.gegidze.vending_machine.enums.UserRole;
import com.gegidze.vending_machine.exception.*;
import com.gegidze.vending_machine.repository.BuyRepository;
import com.gegidze.vending_machine.repository.DepositRepository;
import com.gegidze.vending_machine.repository.ProductRepository;
import com.gegidze.vending_machine.repository.UserRepository;
import com.gegidze.vending_machine.repository.entity.Buy;
import com.gegidze.vending_machine.repository.entity.Product;
import com.gegidze.vending_machine.service.IBuyService;
import com.gegidze.vending_machine.util.Constant;
import com.gegidze.vending_machine.util.Utility;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

/**
 * @Author: s.shakeri
 * at 6/4/2022
 **/

@Service
public class BuyServiceImpl implements IBuyService {

    private final BuyRepository repository;
    private final ProductRepository productRepository;
    private final UserRepository userRepository;
    private final DepositRepository depositRepository;

    public BuyServiceImpl(BuyRepository repository, ProductRepository productRepository, UserRepository userRepository, DepositRepository depositRepository) {
        this.repository = repository;
        this.productRepository = productRepository;
        this.userRepository = userRepository;
        this.depositRepository = depositRepository;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public UserBuyDTO buyProductById(BuyDTO buyDTO) {
        var tempPro = productRepository.findById(buyDTO.getProductId());
        if (tempPro.isPresent()) {
            var usr = userRepository.findByUsername(buyDTO.getUsername());
            if (usr.isPresent()) {
                if (Objects.equals(usr.get().getRole(), UserRole.BUYER)) {
                    var tempDepo = depositRepository.findByUser_IdAndStatus(usr.get().getId(), DepositStatus.CURRENT);
                    if (tempDepo.isPresent() && tempDepo.get().getTotalAmount() >= (tempPro.get().getCost() * buyDTO.getAmount())) {
                        if (tempPro.get().getAvailableAmount() >= buyDTO.getAmount()) {
                            Product product = tempPro.get()
                                    .setAvailableAmount(tempPro.get().getAvailableAmount() - buyDTO.getAmount());
                            productRepository.save(product);
                            depositRepository.save(tempDepo.get()
                                    .setTotalAmount(tempDepo.get().getTotalAmount() - (tempPro.get().getCost() * buyDTO.getAmount())));
                            Buy buy = new Buy()
                                    .setAmount(buyDTO.getAmount())
                                    .setDateTime(Utility.getCurrentDate())
                                    .setProduct(tempPro.get())
                                    .setUser(usr.get());
                            return new UserBuyDTO().from(repository.save(buy));
                        } else
                            throw new InsufficientAmountException(new BaseException.ErrorDetail()
                                    .setLoc("deposit")
                                    .setType(Constant.DEPOSIT_REQUIRED));
                    } else
                        throw new InsufficientDepositException(new BaseException.ErrorDetail()
                                .setLoc("deposit")
                                .setType(Constant.DEPOSIT_REQUIRED));
                } else
                    throw new InvalidRoleException(new BaseException.ErrorDetail()
                            .setLoc("userRole")
                            .setType(Constant.BUYER_USER_ROLE_REQUIRED));
            } else
                throw new NotFoundException(new BaseException.ErrorDetail()
                        .setLoc("username")
                        .setType(Constant.USER_USERNAME_NOT_FOUND));
        } else
            throw new NotFoundException(new BaseException.ErrorDetail()
                    .setLoc("product")
                    .setType(Constant.PRODUCT_NOT_FOUND));
    }
}
