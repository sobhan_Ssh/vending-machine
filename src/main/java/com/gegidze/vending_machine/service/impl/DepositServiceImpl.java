package com.gegidze.vending_machine.service.impl;

import com.gegidze.vending_machine.Dto.request.DepositDTO;
import com.gegidze.vending_machine.Dto.request.ResetDepositDTO;
import com.gegidze.vending_machine.Dto.response.UserDepositDTO;
import com.gegidze.vending_machine.enums.DepositStatus;
import com.gegidze.vending_machine.enums.UserRole;
import com.gegidze.vending_machine.exception.BaseException;
import com.gegidze.vending_machine.exception.DepositAmountNotMultipleOfFiveException;
import com.gegidze.vending_machine.exception.InvalidRoleException;
import com.gegidze.vending_machine.exception.NotFoundException;
import com.gegidze.vending_machine.repository.DepositRepository;
import com.gegidze.vending_machine.repository.UserRepository;
import com.gegidze.vending_machine.repository.entity.Deposit;
import com.gegidze.vending_machine.service.IDepositService;
import com.gegidze.vending_machine.util.Constant;
import com.gegidze.vending_machine.util.Utility;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

/**
 * @Author: s.shakeri
 * at 6/4/2022
 **/

@Service
public class DepositServiceImpl implements IDepositService {

    private final DepositRepository repository;
    private final UserRepository userRepository;

    public DepositServiceImpl(UserRepository userRepository, DepositRepository repository) {
        this.userRepository = userRepository;
        this.repository = repository;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public UserDepositDTO depositByUsername(DepositDTO depositDTORequest) {
        var usr = userRepository.findByUsername(depositDTORequest.getUsername());
        if (usr.isPresent()) {
            if (Objects.equals(usr.get().getRole(), UserRole.BUYER)) {
                if (isDepositValueMultipleOfFive(depositDTORequest.getDepositAmount())) {
                    var temp = repository.findByUser_IdAndStatus(usr.get().getId(), DepositStatus.CURRENT);
                    Deposit currentDeposit = new Deposit();
                    if (temp.isPresent()) {
                        Deposit oldDeposit = repository.save(temp.get().setStatus(DepositStatus.OLD));
                        currentDeposit = repository.save(currentDeposit
                                .setDateTime(Utility.getCurrentDate())
                                .setDepositAmount(depositDTORequest.getDepositAmount())
                                .setPreviousAmount(oldDeposit.getTotalAmount())
                                .setStatus(DepositStatus.CURRENT)
                                .setTotalAmount(depositDTORequest.getDepositAmount() + oldDeposit.getTotalAmount())
                                .setUser(usr.get()));
                    } else {
                        currentDeposit = repository.save(currentDeposit
                                .setDateTime(Utility.getCurrentDate())
                                .setDepositAmount(depositDTORequest.getDepositAmount())
                                .setPreviousAmount(0L)
                                .setStatus(DepositStatus.CURRENT)
                                .setTotalAmount(depositDTORequest.getDepositAmount())
                                .setUser(usr.get()));
                    }
                    return new UserDepositDTO().from(currentDeposit);
                } else
                    throw new DepositAmountNotMultipleOfFiveException(new BaseException.ErrorDetail()
                            .setLoc("depositAmount")
                            .setType(Constant.DEPOSIT_AMOUNT_MULTIPLE_OF_FIVE_REQUIRED));
            } else
                throw new InvalidRoleException(new BaseException.ErrorDetail()
                        .setLoc("userRole")
                        .setType(Constant.BUYER_USER_ROLE_REQUIRED));
        } else
            throw new NotFoundException(new BaseException.ErrorDetail()
                    .setLoc("username")
                    .setType(Constant.USER_USERNAME_NOT_FOUND));
    }

    @Override
    public UserDepositDTO resetDeposit(ResetDepositDTO resetDepositDTO) {
        var usr = userRepository.findByUsername(resetDepositDTO.getUsername());
        if (usr.isPresent()) {
            var temp = repository.findByUser_IdAndStatus(usr.get().getId(), DepositStatus.CURRENT);
            temp.ifPresent(deposit -> repository.save(deposit.setStatus(DepositStatus.OLD)));
            return new UserDepositDTO().from(repository.save(new Deposit()
                    .setStatus(DepositStatus.CURRENT)
                    .setTotalAmount(0L)
                    .setDateTime(Utility.getCurrentDate())
                    .setDepositAmount(0L)
                    .setPreviousAmount(0L)
                    .setUser(usr.get())
            ));
        } else
            throw new NotFoundException(new BaseException.ErrorDetail()
                    .setLoc("username")
                    .setType(Constant.USER_USERNAME_NOT_FOUND));
    }

    private Boolean isDepositValueMultipleOfFive(Long val) {
        return val % 5 == 0;
    }
}
