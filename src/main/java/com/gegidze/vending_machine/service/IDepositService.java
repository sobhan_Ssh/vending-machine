package com.gegidze.vending_machine.service;

import com.gegidze.vending_machine.Dto.request.DepositDTO;
import com.gegidze.vending_machine.Dto.request.ResetDepositDTO;
import com.gegidze.vending_machine.Dto.response.UserDepositDTO;

/**
 * @Author: s.shakeri
 * at 6/4/2022
 **/

public interface IDepositService {

    UserDepositDTO depositByUsername(DepositDTO depositDTO);

    UserDepositDTO resetDeposit(ResetDepositDTO resetDepositDTO);

}
